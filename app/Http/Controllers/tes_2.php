<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class tes_2 extends Controller
{
    //


    public function index()
    {
        $users = DB::table('tes_2')->get();


        return view("tugas_ibuismi.isi.table", ['users' => $users]);
    }
}
