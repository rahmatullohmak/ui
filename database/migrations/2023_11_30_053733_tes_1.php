<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('tes_1', function (Blueprint $table) {

            $table->bigIncrements("id");
            $table->string("nama");
            $table->string("umur");
            $table->string("bio");
            $table->string("alamat");
        });

    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};